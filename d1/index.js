let http = require("http");

// sample database collection

let course =[
		{
			name: "English 101",
			price: 23000,
			isActive: true
		},
		{
			name: "Math 101",
			price: 25000,
			isActive: true
		},
		{
			name: "Reading 101",
			price: 21000,
			isActive: true
		},
];
// sample  database ends here

http.createServer(function(req,res){

	// HTTP Method allow us to identify what action to take for our request
	/*
		With an HTTP Method we can actually routes that caters to the same endpoint 
		but with a different action.

		Most HTTP Methods are primarily concerned with CRUD Operations:

		Common HTTP Method:

			GET = GET method request indicates that the client wants to retrieve 
				  or GET data

			POST = POST method request indicates that the client wants to send data 
			       to create a resource

			PUT = PUT method requests indicates that the client wants to send data to 
			      update a resource

			DELETE = DELETE method request indicates that the client wants to delete 
			        a resource
	*/

	console.log(req.url);//the request URL endpoint
	console.log(req.method);//the method of request

	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a GET method request");
	} else if (req.url === "/" && req.method === "POST"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a POST method request");
	} else if (req.url === "/" && req.method === "PUT"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a PUT method request!");
	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("THis is a response to a DELETE method request!");
	}
	/*
		Mini Activity

		Create a new route for the followiing endpoint: "/courses"
		
		1. This route should be a "GET " method route
			- Add our status code, 200 and our content type: text/plain,
			- Add an end() method to end the response with the following 
			  message:
			  "This is a GET method request for the /course endpoint."
		1. This route should be a "POST " method route
			- Add our status code, 200 and our content type: text/plain,
			- Add an end() method to end the response with the following 
			  message:
			  "This is a POST method request for the /course endpoint."

		use the postman and create request appropriately to test your routes
	*/
		else if(req.url === "/courses" && req.method === "GET"){
			/*
			  Since we are now passing a stringified JSON, so that the client which
			  will receive our responde be able to display our data properly, we have to
			  update our content-type headers to application/json
			  */
			res.writeHead(200,{'Content-Type':'application/json'});
			// we cannot pass another datatype other than a string for out end()
			// to be able to pass our array, we have to transform into a JSON string
			res.end(JSON.stringify(course));
		}else if(req.url === "/courses" && req.method === "POST"){
			/*
			  This route should be able to recevie data from the client and we should be
			  able to create a new course and add it to our courses array
			*/
			// Will act as a placeholder rto contain the data passed from our client.
			let requestBody ="";

			//Receiving data from a client to a nodejs server requires 2 steps:
			/*
				data step - this part will read the strean if data coming from our client
							and process the incoming data into the requestBody variable.
			*/ 

			req.on('data',function(data){

				console.log(data); //stream of data from client
				requestBody += data //data stream is saved into the variable

			})
			// end step - this will run once or after the request data has been comletely sent from our client

			req.on('end',function(){

				// console.log(requestBody); //requestBody now contains data from our postman client
				/* since requestBody is JSON format, we have to parse to add it as an object in our array.*/
				requestBody = JSON.parse(requestBody);

				// Simulaye creating document and adding it in a collection:
				let newCourse ={
					/*
						requestBody is now an object, we will get the name property of the
						requestBody as the value for the name of our newCourse object. 
						Same with price.
					*/
					name: requestBody.name, 
					price: requestBody.price,
					isActive: true // or requestBody.isActive
				}
				console.log(newCourse);
				course.push(newCourse);
				console.log(course); // check if the new course was added to the courses array

				res.writeHead(200,{'Content-Type':'application/json'});
				// send the updated course array to the client as a response.
				res.end(JSON.stringify(course));

			})

			
		}

}).listen(4000);

console.log("Server is running at localhost:4000")